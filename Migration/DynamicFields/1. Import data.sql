IF OBJECT_ID('DynamicFieldsMigration') IS NULL
    BEGIN
        CREATE DATABASE DynamicFieldsMigration
            ON (FILENAME = 'E:\SQLData\DynamicFieldsMigration.mdf')
            FOR ATTACH;
    END
GO


-- BEGIN TRANSACTION
--     CREATE DATABASE QAMetadataMigration
--         ON (FILENAME = '/tmp/QAMetadataMigration.mdf')
--         FOR ATTACH;
-- COMMIT
-- GO
-- 
-- -- USE Migration;
-- 
-- CREATE PROCEDURE dbo.usp_GetPrimaryKeyRange @TableName nvarchar(max), @RangeSize int
-- AS
-- BEGIN
--     DECLARE
--         @sql    nvarchar(max) = 'SELECT @resultOUT = (COUNT(*) + 1) FROM ' + @TableName,
--         @result int
-- 
--     EXECUTE sys.sp_executesql @sql, N'@resultOUT int out', @resultOUT = @result OUTPUT
-- 
--     PRINT 'Result = ' + cast(@result as varchar)
--     RETURN @result
-- END
-- GO


BEGIN TRANSACTION
    USE RSMetadataESDb;

    DECLARE
        @USER      varchar(100) = 'Migration',
        @debugText varchar(100)

    /*
     * DynamicFieldValueCollectionTypes
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldValueCollectionTypes'
        MERGE INTO DynamicFields.DynamicFieldValueCollectionTypes TARGET
        USING DynamicFieldsMigration.DynamicFields.DynamicFieldValueCollectionTypes SRC
        ON TARGET.DynamicFieldValueCollectionTypeName = SRC.DynamicFieldValueCollectionTypeName
        WHEN NOT MATCHED THEN
            INSERT (DynamicFieldValueCollectionTypeNo,
                    DynamicFieldValueCollectionTypeName,
                    IsHierarchical)
            VALUES (SRC.DynamicFieldValueCollectionTypeNo,
                    SRC.DynamicFieldValueCollectionTypeName,
                    SRC.IsHierarchical);

        SELECT *
        FROM DynamicFields.DynamicFieldValueCollectionTypes;
        PRINT 'END MERGE INTO DynamicFieldValueCollectionTypes'
    END

    /*
     * DynamicFieldValueTypes
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldValueTypes'
        MERGE INTO DynamicFields.DynamicFieldValueTypes TARGET
        USING DynamicFieldsMigration.DynamicFields.DynamicFieldValueTypes SRC
        ON TARGET.DynamicFieldValueTypeName = SRC.DynamicFieldValueTypeName
        WHEN NOT MATCHED THEN
            INSERT (DynamicFieldValueTypeNo,
                    DynamicFieldValueTypeName)
            VALUES (SRC.DynamicFieldValueTypeNo,
                    SRC.DynamicFieldValueTypeName);

        SELECT *
        FROM DynamicFields.DynamicFieldValueTypes;
        PRINT 'END MERGE INTO DynamicFieldValueTypes'
    END

    /*
     * DynamicFieldValueOptionStatuses
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldValueOptionStatuses'
        MERGE INTO DynamicFields.DynamicFieldValueOptionStatuses TARGET
        USING DynamicFieldsMigration.DynamicFields.DynamicFieldValueOptionStatuses SRC
        ON TARGET.DynamicFieldValueOptionStatusName = SRC.DynamicFieldValueOptionStatusName
        WHEN NOT MATCHED THEN
            INSERT (DynamicFieldValueOptionStatusNo,
                    DynamicFieldValueOptionStatusName,
                    DynamicFieldValueOptionStatusDescription,
                    CreatedDate,
                    CreatedByUserId)
            VALUES (SRC.DynamicFieldValueOptionStatusNo,
                    SRC.DynamicFieldValueOptionStatusName,
                    SRC.DynamicFieldValueOptionStatusDescription,
                    SYSDATETIME(),
                    @USER);

        SELECT *
        FROM DynamicFields.DynamicFieldValueOptionStatuses;
        PRINT 'END MERGE INTO DynamicFieldValueOptionStatuses'
    END

    /*
     * DynamicFieldValueCollections
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldValueCollections'
        DECLARE
            DFVC_CUR CURSOR FOR
                SELECT DFVC.DynamicFieldValueCollectionNo,
                       DFVCT2.DynamicFieldValueCollectionTypeNo,
                       TARGET.DynamicFieldValueCollectionName,
                       TARGET.DynamicFieldValueCollectionId,
                       TARGET.DynamicFieldValueCollectionStatus,
                       TARGET.DynamicFieldValueCollectionDescription
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldValueCollections TARGET
                         LEFT JOIN
                     DynamicFieldsMigration.DynamicFields.DynamicFieldValueCollectionTypes SRC
                     ON TARGET.DynamicFieldValueCollectionTypeNo = SRC.DynamicFieldValueCollectionTypeNo
                         LEFT JOIN
                     DynamicFields.DynamicFieldValueCollectionTypes DFVCT2
                     ON SRC.DynamicFieldValueCollectionTypeName = DFVCT2.DynamicFieldValueCollectionTypeName
                         LEFT JOIN
                     DynamicFields.DynamicFieldValueCollections DFVC
                     ON TARGET.DynamicFieldValueCollectionId = DFVC.DynamicFieldValueCollectionId;

        DECLARE
            @dfvcNo          int,
            @dfvcTypeNo      int,
            @dfvcName        varchar(100),
            @dfvcId          varchar(100),
            @dfvcStatus      bit,
            @dfvcDescription varchar(100);

        OPEN DFVC_CUR;

        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFVC_CUR INTO
                @dfvcNo,
                @dfvcTypeNo,
                @dfvcName,
                @dfvcId,
                @dfvcStatus,
                @dfvcDescription;

            IF @@FETCH_STATUS <> 0 BREAK;

            SET @debugText = isnull(convert(varchar, @dfvcNo), 'unknown');

            IF @dfvcNo IS NULL
                BEGIN
                    PRINT @debugText + ': Not matched'
                    EXECUTE @dfvcNo = usp_GetPrimaryKeyRange @TableName='DynamicFieldValueCollections', @RangeSize = 1;

                    INSERT INTO DynamicFields.DynamicFieldValueCollections
                    VALUES (@dfvcNo, @dfvcTypeNo, @dfvcName, @dfvcId, @dfvcStatus, @dfvcDescription);
                END
            ELSE
                BEGIN
                    PRINT @debugText + ': Not matched'

                    UPDATE DynamicFields.DynamicFieldValueCollections
                    SET DynamicFieldValueCollectionTypeNo      = @dfvcTypeNo,
                        DynamicFieldValueCollectionDescription = @dfvcDescription,
                        DynamicFieldValueCollectionStatus      = @dfvcStatus
                    WHERE DynamicFieldValueCollectionNo = @dfvcNo;
                END

        END

        CLOSE DFVC_CUR;
        DEALLOCATE DFVC_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldValueCollections;
        PRINT 'END MERGE INTO DynamicFieldValueCollections'
    END

    /*
     * DynamicFieldEntities
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldEntities'
        DECLARE
            DFE_CUR CURSOR FOR
                SELECT NEW.DynamicFieldEntityNo,
                       THIS.EntityTableName,
                       THIS.FieldsTableName,
                       THIS.CreatedDate,
                       THIS.CreatedByUserId,
                       THIS.Status,
                       NEW_PARENT.DynamicFieldEntityNo as ParentDynamicFieldEntityNo
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldEntities THIS
                         LEFT JOIN
                     DynamicFieldsMigration.DynamicFields.DynamicFieldEntities OLD_PARENT
                     ON THIS.ParentDynamicFieldEntityNo = OLD_PARENT.DynamicFieldEntityNo
                         LEFT JOIN DynamicFields.DynamicFieldEntities NEW
                                   ON NEW.FieldsTableName = THIS.FieldsTableName AND
                                      NEW.EntityTableName = THIS.EntityTableName
                         LEFT JOIN DynamicFields.DynamicFieldEntities NEW_PARENT
                                   ON NEW_PARENT.FieldsTableName = OLD_PARENT.FieldsTableName AND
                                      NEW_PARENT.EntityTableName = OLD_PARENT.EntityTableName
                ORDER BY THIS.ParentDynamicFieldEntityNo;

        DECLARE
            @dfeEntityNo        int,
            @dfeEntityTableName varchar(100),
            @dfeFieldsTableName varchar(100),
            @dfeCreatedDate     datetime,
            @dfeCreatedByUserId varchar(255),
            @dfeStatus          bit,
            @dfeParentEntityNo  int;

        OPEN DFE_CUR;

        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFE_CUR INTO
                @dfeEntityNo,
                @dfeEntityTableName,
                @dfeFieldsTableName,
                @dfeCreatedDate,
                @dfeCreatedByUserId,
                @dfeStatus,
                @dfeParentEntityNo;

            IF @@FETCH_STATUS <> 0 BREAK;

            set @debugText = ISNULL(CONVERT(varchar, @dfeEntityNo), 'unknown');

            IF @dfeEntityNo IS NULL
                BEGIN
                    PRINT @debugText + ': Not matched';

                    EXECUTE @dfeEntityNo = usp_GetPrimaryKeyRange @TableName='DynamicFieldEntities', @RangeSize = 1;

                    INSERT INTO DynamicFields.DynamicFieldEntities
                    VALUES (@dfeEntityNo,
                            @dfeEntityTableName,
                            @dfeFieldsTableName,
                            SYSDATETIME(),
                            null,
                            @USER,
                            null,
                            @dfeParentEntityNo,
                            @dfeStatus)
                END
            ELSE
                BEGIN
                    UPDATE DynamicFields.DynamicFieldEntities
                    SET ModifiedDate               = SYSDATETIME(),
                        ModifiedByUserId           = @USER,
                        Status                     = @dfeStatus,
                        ParentDynamicFieldEntityNo = @dfeParentEntityNo
                    WHERE EntityTableName = @dfeEntityTableName
                      AND FieldsTableName = @dfeFieldsTableName
                END
        END

        CLOSE DFE_CUR;
        DEALLOCATE DFE_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldEntities;
        PRINT 'END MERGE INTO DynamicFieldEntities'
    END

    /*
     * DynamicFieldsDefinitions
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldsDefinitions'
        DECLARE
            DFD_CUR CURSOR FOR
                SELECT DFD_NEW.DynamicFieldDefinitionNo,
                       DFVT.DynamicFieldValueTypeName,
                       DFVC.DynamicFieldValueCollectionId,
                       DFD.DynamicFieldDefinitionId,
                       DFD.DynamicFieldDefinitionName,
                       DFD.IsRequired,
                       DFD.SequenceNumber,
                       DFD.DefaultValue,
                       DFD.CreatedDate,
                       DFD.CreatedByUserId,
                       DFD.Enabled,
                       DFD.Visible,
                       DFD.VisibleInGrid,
                       DFD.Searchable,
                       DFD.ReadOnly,
                       DFD.MultiSelect,
                       DFD.RestrictedToAvailableValues,
                       DFD.TextEditRowCount,
                       DFD.ValueRegEx,
                       DFD.Description,
                       DFD.IsVisibleAtHQ,
                       DFD.Status,
                       DFD.DoExport,
                       DFD.DoImport,
                       DFD.ShowInLists,
                       DFD.NumberOfMemoLines,
                       DFD.StringLength,
                       DFD.IsInheritable,
                       DFD.PrintLabel,
                       DFD.IsSystemDefined,
                       MT.MetadataTypeId,
                       DFD.ScriptValue
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldDefinitions DFD
                         LEFT JOIN
                     DynamicFieldsMigration.DynamicFields.DynamicFieldValueTypes DFVT
                     ON DFD.DynamicFieldValueTypeNo = DFVT.DynamicFieldValueTypeNo
                         LEFT JOIN
                     DynamicFieldsMigration.DynamicFields.DynamicFieldValueCollections DFVC
                     ON DFD.DynamicFieldValueCollectionNo = DFVC.DynamicFieldValueCollectionNo
                         LEFT JOIN
                     DynamicFieldsMigration.Metadata.MetadataTypes MT
                     ON MT.MetadataTypeNo = DFD.MetadataTypeNo
                         LEFT JOIN DynamicFields.DynamicFieldDefinitions DFD_NEW
                                   ON DFD_NEW.DynamicFieldDefinitionId = DFD.DynamicFieldDefinitionId;

        DECLARE
            @dfdNo                          int,
            @dfdValueTypeName               varchar(100),
            @dfdValueCollectionId           varchar(100),
            @dfdId                          varchar(255),
            @dfdName                        varchar(100),
            @dfdIsRequired                  bit,
            @dfdSequenceNumber              smallint,
            @dfdDefaultValue                varchar(100),
            @dfdCreatedDate                 datetime,
            @dfdCreatedByUserId             varchar(255),
            @dfdEnabled                     bit,
            @dfdVisible                     bit,
            @dfdVisibleInGrid               bit,
            @dfdSearchable                  bit,
            @dfdReadOnly                    bit,
            @dfdMultiSelect                 bit,
            @dfdRestrictedToAvailableValues bit,
            @dfdTextRowCount                int,
            @dfdValueRegEx                  varchar(8000),
            @dfdDescription                 varchar(8000),
            @dfdIsVisibleAtHQ               bit,
            @dfdStatus                      bit,
            @dfdDoExport                    bit,
            @dfdDoImport                    bit,
            @dfdShowInLists                 bit,
            @dfdNumberOfMemoLines           tinyint,
            @dfdStringLength                nchar(10),
            @dfdIsInheritable               bit,
            @dfdPrintLabel                  bit,
            @dfdIsSystemDefined             bit,
            @dfdMetadataTypeId              varchar(200),
            @dfdScriptValue                 nvarchar(max);

        OPEN DFD_CUR;

        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFD_CUR INTO
                @dfdNo,
                @dfdValueTypeName,
                @dfdValueCollectionId,
                @dfdId,
                @dfdName,
                @dfdIsRequired,
                @dfdSequenceNumber,
                @dfdDefaultValue,
                @dfdCreatedDate,
                @dfdCreatedByUserId,
                @dfdEnabled,
                @dfdVisible,
                @dfdVisibleInGrid,
                @dfdSearchable,
                @dfdReadOnly,
                @dfdMultiSelect,
                @dfdRestrictedToAvailableValues,
                @dfdTextRowCount,
                @dfdValueRegEx,
                @dfdDescription,
                @dfdIsVisibleAtHQ,
                @dfdStatus,
                @dfdDoExport,
                @dfdDoImport,
                @dfdShowInLists,
                @dfdNumberOfMemoLines,
                @dfdStringLength,
                @dfdIsInheritable,
                @dfdPrintLabel,
                @dfdIsSystemDefined,
                @dfdMetadataTypeId,
                @dfdScriptValue;

            DECLARE
                @dfdValueTypeNo       int = (
                    SELECT DynamicFieldValueTypeNo
                    FROM DynamicFields.DynamicFieldValueTypes
                    WHERE DynamicFieldValueTypeName = @dfdValueTypeName
                ),
                @dfdValueCollectionNo int = (
                    SELECT DynamicFieldValueCollectionNo
                    FROM DynamicFields.DynamicFieldValueCollections
                    WHERE DynamicFieldValueCollectionId = @dfdValueCollectionId
                ),
                @dfdMetadataTypeNo    int = (
                    SELECT MetadataTypeNo
                    FROM Metadata.MetadataTypes
                    WHERE MetadataTypeId = @dfdMetadataTypeId
                );

            IF @@FETCH_STATUS <> 0
                BREAK;

            SET @debugText = ISNULL(CONVERT(varchar, @dfdNo), 'unknown');

            IF @dfdNo IS NULL
                BEGIN
                    PRINT @debugText + ': Not matched'
                    EXECUTE @dfdNo = usp_GetPrimaryKeyRange @TableName = 'DynamicFieldDefinitions', @RangeSize = 1;

                    INSERT INTO DynamicFields.DynamicFieldDefinitions (DynamicFieldDefinitionNo,
                                                                       DynamicFieldValueTypeNo,
                                                                       DynamicFieldValueCollectionNo,
                                                                       DynamicFieldDefinitionId,
                                                                       DynamicFieldDefinitionName,
                                                                       IsRequired,
                                                                       SequenceNumber,
                                                                       DefaultValue,
                                                                       CreatedDate,
                                                                       ModifiedDate,
                                                                       CreatedByUserId,
                                                                       ModifiedByUserId,
                                                                       Enabled,
                                                                       Visible,
                                                                       VisibleInGrid,
                                                                       Searchable,
                                                                       ReadOnly,
                                                                       MultiSelect,
                                                                       RestrictedToAvailableValues,
                                                                       TextEditRowCount,
                                                                       ValueRegEx,
                                                                       Description,
                                                                       IsVisibleAtHQ,
                                                                       Status,
                                                                       DoExport,
                                                                       DoImport,
                                                                       ShowInLists,
                                                                       NumberOfMemoLines,
                                                                       StringLength,
                                                                       IsInheritable,
                                                                       PrintLabel,
                                                                       IsSystemDefined,
                                                                       MetadataTypeNo,
                                                                       ScriptValue)
                    VALUES (@dfdNo,
                            @dfdValueTypeNo,
                            @dfdValueCollectionNo,
                            @dfdId,
                            @dfdName,
                            @dfdIsRequired,
                            @dfdSequenceNumber,
                            @dfdDefaultValue,
                            @dfdCreatedDate,
                            SYSDATETIME(),
                            @dfdCreatedByUserId,
                            @USER,
                            @dfdEnabled,
                            @dfdVisible,
                            @dfdVisibleInGrid,
                            @dfdSearchable,
                            @dfdReadOnly,
                            @dfdMultiSelect,
                            @dfdRestrictedToAvailableValues,
                            @dfdTextRowCount,
                            @dfdValueRegEx,
                            @dfdDescription,
                            @dfdIsVisibleAtHQ,
                            @dfdStatus,
                            @dfdDoExport,
                            @dfdDoImport,
                            @dfdShowInLists,
                            @dfdNumberOfMemoLines,
                            @dfdStringLength,
                            @dfdIsInheritable,
                            @dfdPrintLabel,
                            @dfdIsSystemDefined,
                            @dfdMetadataTypeNo,
                            @dfdScriptValue);
                END
            ELSE
                BEGIN
                    PRINT @debugText + ': Matched'

                    UPDATE RSMetadataESDb.DynamicFields.DynamicFieldDefinitions

                    SET DynamicFieldValueTypeNo       = @dfdValueTypeNo,
                        DynamicFieldValueCollectionNo = @dfdValueCollectionNo,
                        DynamicFieldDefinitionName    = @dfdName,
                        IsRequired                    = @dfdIsRequired,
                        SequenceNumber                = @dfdSequenceNumber,
                        DefaultValue                  = @dfdDefaultValue,
                        ModifiedDate                  = sysdatetime(),
                        ModifiedByUserId              = @USER,
                        Enabled                       = @dfdEnabled,
                        Visible                       = @dfdVisible,
                        VisibleInGrid                 = @dfdVisibleInGrid,
                        Searchable                    = @dfdSearchable,
                        ReadOnly                      = @dfdReadOnly,
                        MultiSelect                   = @dfdMultiSelect,
                        RestrictedToAvailableValues   = @dfdRestrictedToAvailableValues,
                        TextEditRowCount              = @dfdTextRowCount,
                        ValueRegEx                    = @dfdValueRegEx,
                        Description                   = @dfdDescription,
                        IsVisibleAtHQ                 = @dfdIsVisibleAtHQ,
                        Status                        = @dfdStatus,
                        DoExport                      = @dfdDoExport,
                        DoImport                      = @dfdDoImport,
                        ShowInLists                   = @dfdShowInLists,
                        NumberOfMemoLines             = @dfdNumberOfMemoLines,
                        StringLength                  = @dfdStringLength,
                        IsInheritable                 = @dfdIsInheritable,
                        PrintLabel                    = @dfdPrintLabel,
                        IsSystemDefined               = @dfdIsSystemDefined,
                        MetadataTypeNo                = @dfdMetadataTypeNo,
                        ScriptValue                   = @dfdScriptValue
                    WHERE DynamicFieldDefinitionNo = @dfdNo;
                END
        END

        CLOSE DFD_CUR;
        DEALLOCATE DFD_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldDefinitions;
        PRINT 'END MERGE INTO DynamicFieldsDefinitions'
    END

    /*
     * DynamicFieldValueOptions
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldValueOptions'
        DECLARE
            DFVO_CUR CURSOR FOR
                SELECT NEW_DFVO.DynamicFieldValueOptionNo,
                       NEW_PARENT.DynamicFieldValueOptionNo AS ParentDynamicFieldValueOptionNo,
                       NEW_DFVC.DynamicFieldValueCollectionNo,
                       THIS.Text,
                       THIS.IsUserDefined,
                       THIS.Value,
                       THIS.DynamicFieldValueOptionStatusNo,
                       THIS.CreatedDate,
                       THIS.DeletedDate
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldValueOptions THIS
                         LEFT JOIN DynamicFieldsMigration.DynamicFields.DynamicFieldValueCollections DFVC
                                   ON THIS.DynamicFieldValueCollectionNo = DFVC.DynamicFieldValueCollectionNo
                         LEFT JOIN DynamicFields.DynamicFieldValueCollections NEW_DFVC
                                   ON NEW_DFVC.DynamicFieldValueCollectionId = DFVC.DynamicFieldValueCollectionId
                         LEFT JOIN DynamicFields.DynamicFieldValueOptions NEW_DFVO
                                   ON NEW_DFVC.DynamicFieldValueCollectionNo =
                                      NEW_DFVO.DynamicFieldValueCollectionNo AND
                                      NEW_DFVO.Text = THIS.Text
                         LEFT JOIN DynamicFieldsMigration.DynamicFields.DynamicFieldValueOptions PARENT
                                   ON THIS.ParentDynamicFieldValueOptionNo = PARENT.DynamicFieldValueOptionNo
                         LEFT JOIN DynamicFields.DynamicFieldValueOptions NEW_PARENT ON NEW_PARENT.TEXT = Parent.Text
                ORDER BY ParentDynamicFieldValueOptionNo, DynamicFieldValueOptionNo desc


        DECLARE
            @dfvoNo            int,
            @dfvoParentNo      int,
            @dfvoCollectionNo  varchar(100),
            @dfvoText          varchar(100),
            @dfvoIsUserDefined varchar(100),
            @dfvoValue         varchar(max),
            @dfvoStatusNo      smallint,
            @dfvoCreatedDate   datetime,
            @dfvoDeletedDate   datetime;

        OPEN DFVO_CUR;

        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFVO_CUR INTO
                @dfvoNo,
                @dfvoParentNo,
                @dfvoCollectionNo,
                @dfvoText,
                @dfvoIsUserDefined,
                @dfvoValue,
                @dfvoStatusNo,
                @dfvoCreatedDate,
                @dfvoDeletedDate;

            IF @@FETCH_STATUS <> 0 BREAK;


            set @debugText = isnull(convert(varchar, @dfvoNo), 'unknown');

            IF @dfvoNo IS NULL
                BEGIN
                    PRINT @debugText + ': Not matched'
                    EXECUTE @dfvoNo = usp_GetPrimaryKeyRange @TableName = 'DynamicFieldValueOptions', @RangeSize = 1;

                    INSERT INTO DynamicFields.DynamicFieldValueOptions
                    VALUES (@dfvoNo, @dfvoParentNo, @dfvoCollectionNo, @dfvoText, @dfvoIsUserDefined, @dfvoValue,
                            @dfvoStatusNo, sysdatetime(), sysdatetime(), sysdatetime());

                END
            ELSE
                BEGIN
                    PRINT @debugText + ': Matched'
                    UPDATE DynamicFields.DynamicFieldValueOptions
                    SET ParentDynamicFieldValueOptionNo = @dfvoParentNo,
                        DynamicFieldValueCollectionNo   = @dfvoCollectionNo,
                        Text                            = @dfvoText,
                        IsUserDefined                   = @dfvoIsUserDefined,
                        Value                           = @dfvoValue,
                        DynamicFieldValueOptionStatusNo = @dfvoStatusNo,
                        ModifiedDate                    = SYSDATETIME(),
                        DeletedDate                     = @dfvoDeletedDate
                    WHERE DynamicFields.DynamicFieldValueOptions.DynamicFieldValueOptionNo = @dfvoNo
                END
        END

        CLOSE DFVO_CUR;
        DEALLOCATE DFVO_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldValueOptions;
        PRINT 'END MERGE INTO DynamicFieldValueOptions'
    END

    /*
     * DynamicFieldGroups
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldGroups'
        DECLARE
            DFG_CUR CURSOR FOR
                SELECT NEW.DynamicFieldGroupNo,
                       THIS.Name,
                       THIS.Description,
                       THIS.SequenceNumber,
                       THIS.CreatedDate,
                       THIS.CreatedByUserId,
                       THIS.MaxFieldWidth,
                       THIS.NumberOfColumns,
                       THIS.ShowCaptionOnTop,
                       THIS.IsDefaultGroup,
                       THIS.IsHorisontaleOrientation,
                       THIS.ShowGroupCaption,
                       THIS.Status,
                       THIS.DynamicFieldGroupId,
                       THIS.IsDynamicFieldsAsFixedEnabled,
                       NEW_PARENT.DynamicFieldGroupNo
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldGroups THIS
                         LEFT JOIN
                     DynamicFieldsMigration.DynamicFields.DynamicFieldGroups PARENT
                     ON THIS.ParentDynamicFieldGroupNo = PARENT.DynamicFieldGroupNo
                         LEFT JOIN DynamicFields.DynamicFieldGroups NEW
                                   ON NEW.DynamicFieldGroupId = THIS.DynamicFieldGroupId
                         LEFT JOIN DynamicFields.DynamicFieldGroups NEW_PARENT
                                   ON NEW_PARENT.DynamicFieldGroupId = PARENT.DynamicFieldGroupId
                ORDER BY THIS.ParentDynamicFieldGroupNo;

        DECLARE
            @dfgNo                            int,
            @dfgName                          varchar(500),
            @dfgDescription                   varchar(500),
            @dfgSequenceNumber                int,
            @dfgCreatedDate                   datetime,
            @dfgCreatedByUserId               varchar(255),
            @dfgMaxFieldWidth                 smallint,
            @dfgNumberOfColumns               tinyint,
            @dfgShowCaptionOnTop              bit,
            @dfgIsDefaultGroup                bit,
            @dfgIsHorisontaleOrientation      bit,
            @dfgShowGroupCaption              bit,
            @dfgStatus                        bit,
            @dfgId                            varchar(50),
            @dfgParentNo                      int,
            @dfgIsDynamicFieldsAsFixedEnabled bit;

        OPEN DFG_CUR;


        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFG_CUR INTO
                @dfgNo,
                @dfgName,
                @dfgDescription,
                @dfgSequenceNumber,
                @dfgCreatedDate,
                @dfgCreatedByUserId,
                @dfgMaxFieldWidth,
                @dfgNumberOfColumns,
                @dfgShowCaptionOnTop,
                @dfgIsDefaultGroup,
                @dfgIsHorisontaleOrientation,
                @dfgShowGroupCaption,
                @dfgStatus,
                @dfgId,
                @dfgIsDynamicFieldsAsFixedEnabled,
                @dfgParentNo;

            IF @@FETCH_STATUS <> 0 BREAK;

            set @debugText = isnull(convert(varchar, @dfgNo), 'unknown');

            IF @dfgNo IS NULL
                BEGIN
                    PRINT @debugText + ': Not matched'
                    EXECUTE @dfgNo = usp_GetPrimaryKeyRange @TableName = 'DynamicFieldGroups', @RangeSize = 1;

                    INSERT INTO DynamicFields.DynamicFieldGroups
                    VALUES (@dfgNo,
                            @dfgName,
                            @dfgDescription,
                            @dfgSequenceNumber,
                            SYSDATETIME(),
                            @USER,
                            null,
                            null,
                            @dfgParentNo,
                            @dfgMaxFieldWidth,
                            @dfgNumberOfColumns,
                            @dfgShowCaptionOnTop,
                            @dfgIsDefaultGroup,
                            @dfgIsHorisontaleOrientation,
                            @dfgShowGroupCaption,
                            @dfgStatus,
                            @dfgId,
                            @dfgIsDynamicFieldsAsFixedEnabled)

                END
            ELSE
                BEGIN
                    PRINT @debugText + ': Matched'

                    UPDATE DynamicFields.DynamicFieldGroups
                    SET Name                          = @dfgName,
                        Description                   = @dfgDescription,
                        ModifiedDate                  = SYSDATETIME(),
                        ModifiedByUserId              = @USER,
                        ParentDynamicFieldGroupNo     = @dfgParentNo,
                        MaxFieldWidth                 = @dfgMaxFieldWidth,
                        NumberOfColumns               = @dfgNumberOfColumns,
                        ShowCaptionOnTop              = @dfgShowCaptionOnTop,
                        IsDefaultGroup                = @dfgIsDefaultGroup,
                        IsHorisontaleOrientation      = @dfgIsHorisontaleOrientation,
                        ShowGroupCaption              = @dfgShowCaptionOnTop,
                        Status                        = @dfgStatus,
                        IsDynamicFieldsAsFixedEnabled = @dfgIsDynamicFieldsAsFixedEnabled
                    WHERE DynamicFieldGroupNo = @dfgNo
                END
        END

        CLOSE DFG_CUR;
        DEALLOCATE DFG_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldGroups;
        PRINT 'END MERGE INTO DynamicFieldGroups'
    END

    /*
     * DynamicFieldDefinitionEntities
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldDefinitionEntities'
        DECLARE
            DFDE_CUR CURSOR FOR
                SELECT DFD.DynamicFieldDefinitionId,
                       DFE.EntityTableName,
                       DFE.FieldsTableName
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldDefinitionEntities DFDE,
                     DynamicFieldsMigration.DynamicFields.DynamicFieldEntities DFE,
                     DynamicFieldsMigration.DynamicFields.DynamicFieldDefinitions DFD
                WHERE DFDE.DynamicFieldEntityNo = DFE.DynamicFieldEntityNo
                  AND DFDE.DynamicFieldDefinitionNo = DFD.DynamicFieldDefinitionNo;

        DECLARE
            @dfdeDefinitionId    varchar(255),
            @dfdeEntityTableName varchar(100),
            @dfdeFieldsTableName varchar(100);

        OPEN DFDE_CUR;

        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFDE_CUR INTO
                @dfdeDefinitionId,
                @dfdeEntityTableName,
                @dfdeFieldsTableName;

            IF @@FETCH_STATUS <> 0 BREAK

            MERGE INTO DynamicFields.DynamicFieldDefinitionEntities TARGET
            USING
                (SELECT (
                            SELECT DynamicFieldDefinitionNo
                            FROM DynamicFields.DynamicFieldDefinitions
                            WHERE DynamicFieldDefinitionId = @dfdeDefinitionId
                        ) as DynamicFieldDefinitionNo,
                        (
                            SELECT DynamicFieldEntityNo
                            FROM DynamicFields.DynamicFieldEntities
                            WHERE EntityTableName = @dfdeEntityTableName
                              AND FieldsTableName = @dfdeFieldsTableName
                        ) as DynamicFieldEntityNo) SRC
            ON TARGET.DynamicFieldDefinitionNo = SRC.DynamicFieldDefinitionNo AND
               TARGET.DynamicFieldEntityNo = SRC.DynamicFieldEntityNo
            WHEN NOT MATCHED THEN
                INSERT (DynamicFieldEntityNo,
                        DynamicFieldDefinitionNo)
                VALUES (SRC.DynamicFieldEntityNo,
                        SRC.DynamicFieldDefinitionNo);
        END


        CLOSE DFDE_CUR;
        DEALLOCATE DFDE_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldDefinitionEntities;
        PRINT 'END MERGE INTO DynamicFieldDefinitionEntities'
    END

    /*
     * DynamicFieldEntityGroups
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldEntityGroups'
        DECLARE
            DFEG_CUR CURSOR FOR
                SELECT NEW_DFEG.DynamicFieldEntityGroup,
                       NEW_DFE.DynamicFieldEntityNo,
                       NEW_DFG.DynamicFieldGroupNo
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldEntityGroups OLD_DFEG
                         LEFT JOIN DynamicFieldsMigration.DynamicFields.DynamicFieldEntities OLD_DFE
                                   ON OLD_DFE.DynamicFieldEntityNo = OLD_DFEG.DynamicFieldEntityNo
                         LEFT JOIN DynamicFieldsMigration.DynamicFields.DynamicFieldGroups OLD_DFG
                                   ON OLD_DFEG.DynamicFieldGroupNo = OLD_DFG.DynamicFieldGroupNo
                         LEFT JOIN DynamicFields.DynamicFieldEntities NEW_DFE
                                   ON NEW_DFE.FieldsTableName = OLD_DFE.FieldsTableName
                                       AND NEW_DFE.EntityTableName = OLD_DFE.FieldsTableName
                         LEFT JOIN dynamicFields.DynamicFieldGroups NEW_DFG
                                   ON NEW_DFG.DynamicFieldGroupId = OLD_DFG.DynamicFieldGroupId
                         LEFT JOIN DynamicFields.DynamicFieldEntityGroups NEW_DFEG
                                   ON NEW_DFE.DynamicFieldEntityNo = NEW_DFEG.DynamicFieldEntityNo
                                       AND NEW_DFG.DynamicFieldGroupNo = NEW_DFEG.DynamicFieldGroupNo

        DECLARE
            @dfegNo       int,
            @dfegGroupNo  int,
            @dfegEntityNo int;

        OPEN DFEG_CUR;

        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFEG_CUR INTO
                @dfegNo,
                @dfegEntityNo,
                @dfegGroupNo

            IF @@FETCH_STATUS <> 0 BREAK

            SET @debugText = isnull(convert(varchar, @dfegNo), 'unknown');

            IF @dfegNo IS NULL
                BEGIN
                    PRINT @debugText + ': Not matched'

--                     EXECUTE @dfegNo = usp_GetPrimaryKeyRange @TableName = 'DynamicFieldEntityGroups', @RangeSize = 1;

                    INSERT INTO DynamicFields.DynamicFieldEntityGroups
                    VALUES (@dfegGroupNo, @dfegEntityNo)
                END
            ELSE
                PRINT @debugText + ': Matched. Skipping.'
        END


        CLOSE DFEG_CUR;
        DEALLOCATE DFEG_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldGroups;
        PRINT 'END MERGE INTO DynamicFieldEntityGroups'
    END

    /*
     * DynamicFieldDefinitionGroups
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldDefinitionGroups'
        DECLARE
            DFDG_CUR CURSOR FOR
                SELECT DFD.DynamicFieldDefinitionId,
                       DFG.DynamicFieldGroupId
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldDefinitionGroups DFDG
                         LEFT JOIN DynamicFieldsMigration.DynamicFields.DynamicFieldDefinitions DFD
                                   ON DFDG.DynamicFieldDefinitionNo = DFD.DynamicFieldDefinitionNo
                         LEFT JOIN DynamicFieldsMigration.DynamicFields.DynamicFieldGroups DFG
                                   ON DFDG.DynamicFieldGroupNo = DFG.DynamicFieldGroupNo;

        DECLARE
            @dfdgDefinitionId varchar(255),
            @dfdgGroupId      varchar(50)

        OPEN DFDG_CUR;

        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFDG_CUR INTO
                @dfdgDefinitionId,
                @dfdgGroupId;

            IF @@FETCH_STATUS <> 0
                BEGIN
                    BREAK
                end

            MERGE INTO DynamicFields.DynamicFieldDefinitionGroups TARGET
            USING (SELECT (
                              SELECT DynamicFieldDefinitionNo
                              FROM DynamicFields.DynamicFieldDefinitions
                              WHERE DynamicFieldDefinitionId = @dfdgDefinitionId
                          ) as DynamicFieldDefinitionNo,
                          (
                              SELECT DynamicFieldGroupNo
                              FROM DynamicFields.DynamicFieldGroups
                              WHERE DynamicFieldGroupId = @dfdgGroupId
                          ) as DynamicFieldGroupNo
            ) SRC
            ON
                    TARGET.DynamicFieldDefinitionNo = SRC.DynamicFieldDefinitionNo
                    AND TARGET.DynamicFieldGroupNo = SRC.DynamicFieldGroupNo
            WHEN NOT MATCHED THEN
                INSERT (DynamicFieldDefinitionNo, DynamicFieldGroupNo)
                VALUES (SRC.DynamicFieldDefinitionNo, SRC.DynamicFieldGroupNo);
        END

        CLOSE DFDG_CUR;
        DEALLOCATE DFDG_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldDefinitionGroups;
        PRINT 'END MERGE INTO DynamicFieldDefinitionGroups'
    END

    /*
     * DynamicFieldGroupRoles
     */
    BEGIN
        PRINT 'BEGIN MERGE INTO DynamicFieldGroupRoles'
        DECLARE
            DFGR_CUR CURSOR FOR
                SELECT DynamicFieldGroupId,
                       RoleId
                FROM DynamicFieldsMigration.DynamicFields.DynamicFieldGroupRoles DFGR
                         LEFT JOIN DynamicFieldsMigration.DynamicFields.DynamicFieldGroups DFG
                                   ON DFGR.DynamicFieldGroupNo = DFG.DynamicFieldGroupNo;

        DECLARE
            @dfgrId      varchar(255),
            @dfgrGroupId varchar(50);

        OPEN DFGR_CUR;


        WHILE (1 = 1)
        BEGIN
            FETCH NEXT FROM DFGR_CUR INTO
                @dfgrGroupId,
                @dfgrId;

            IF @@FETCH_STATUS <> 0 BREAK


            MERGE INTO DynamicFields.DynamicFieldGroupRoles TARGET
            USING (SELECT (
                              SELECT DynamicFieldGroupNo
                              FROM DynamicFields.DynamicFieldGroups
                              WHERE DynamicFieldGroupId = @dfgrGroupId
                          )       as DynamicFieldGroupNo,
                          @dfgrId as RoleId
            ) SRC
            ON TARGET.RoleId = SRC.RoleId
            WHEN NOT MATCHED THEN
                INSERT (DynamicFieldGroupNo,
                        RoleId)
                values (SRC.DynamicFieldGroupNo,
                        SRC.RoleId)
            WHEN MATCHED THEN
                UPDATE SET TARGET.DynamicFieldGroupNo = SRC.DynamicFieldGroupNo;
        end

        CLOSE DFGR_CUR;
        DEALLOCATE DFGR_CUR;

        SELECT *
        FROM DynamicFields.DynamicFieldGroupRoles;
        PRINT 'END MERGE INTO DynamicFieldGroupRoles'
    END
COMMIT
