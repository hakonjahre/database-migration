IF DB_ID('DynamicFieldsMigration') IS NULL
    BEGIN
        PRINT 'Creating or attaching MigrateApplicationParameters'

        CREATE DATABASE DynamicFieldsMigration ON
            (
                FILENAME = 'E:\SQLDATA\DynamicFieldsMigration.mdf',
                NAME = 'DynamicFieldsMigration'
                )
    END
ELSE
    PRINT 'DynamicFieldsMigration exists'
GO

BEGIN TRANSACTION
    USE DynamicFieldsMigration;

    IF NOT EXISTS(SELECT schema_name
                  FROM INFORMATION_SCHEMA.SCHEMATA
                  WHERE SCHEMA_NAME = 'DynamicFields')
        BEGIN
            PRINT 'Create schema: DynamicFields'
            EXEC sys.sp_executesql N'CREATE SCHEMA DynamicFields';
        END

    SELECT *
    INTO DynamicFields.DynamicFieldValueCollectionTypes
    FROM RSMetadataESDb.DynamicFields.DynamicFieldValueCollectionTypes;

    SELECT *
    INTO DynamicFields.DynamicFieldValueTypes
    FROM RSMetadataESDb.DynamicFields.DynamicFieldValueTypes;

    SELECT *
    INTO DynamicFields.DynamicFieldValueCollections
    FROM RSMetadataESDb.DynamicFields.DynamicFieldValueCollections;

    SELECT *
    INTO DynamicFields.DynamicFieldValueOptionStatuses
    FROM RSMetadataESDb.DynamicFields.DynamicFieldValueOptionStatuses;

    SELECT *
    INTO DynamicFields.DynamicFieldDefinitions
    FROM RSMetadataESDb.DynamicFields.DynamicFieldDefinitions;

    SELECT *
    INTO DynamicFields.DynamicFieldValueOptions
    FROM RSMetadataESDb.DynamicFields.DynamicFieldValueOptions;

    SELECT *
    INTO DynamicFields.DynamicFieldGroups
    FROM RSMetadataESDb.DynamicFields.DynamicFieldGroups;

    SELECT *
    INTO DynamicFields.DynamicFieldEntities
    FROM RSMetadataESDb.DynamicFields.DynamicFieldEntities;

    SELECT *
    INTO DynamicFields.DynamicFieldGroupRoles
    FROM RSMetadataESDb.DynamicFields.DynamicFieldGroupRoles;

    SELECT *
    INTO DynamicFields.DynamicFieldEntityGroups
    FROM RSMetadataESDb.DynamicFields.DynamicFieldEntityGroups;

    SELECT *
    INTO DynamicFields.DynamicFieldDefinitionGroups
    FROM RSMetadataESDb.DynamicFields.DynamicFieldDefinitionGroups;

    SELECT *
    INTO DynamicFields.DynamicFieldDefinitionEntities
    FROM RSMetadataESDb.DynamicFields.DynamicFieldDefinitionEntities

-- Metadata
    IF NOT EXISTS(SELECT schema_name
                  FROM INFORMATION_SCHEMA.SCHEMATA
                  WHERE SCHEMA_NAME = 'Metadata')
        BEGIN
            EXEC sys.sp_executesql N'CREATE SCHEMA Metadata';
        END
    SELECT *
    INTO Metadata.MetadataStatuses
    FROM RSMetadataESDb.Metadata.MetadataStatuses;

    SELECT *
    INTO Metadata.MetadataExports
    FROM RSMetadataESDb.Metadata.MetadataExports;

    SELECT *
    INTO Metadata.MetadataTypes
    FROM RSMetadataESDb.Metadata.MetadataTypes;

    SELECT *
    INTO Metadata.Metadatas
    FROM RSMetadataESDb.Metadata.Metadatas;

    SELECT *
    INTO Metadata.MetadataExportMembers
    FROM RSMetadataESDb.Metadata.MetadataExportMembers;

    SELECT *
    INTO Metadata.MetadataDynamicFields
    FROM RSMetadataESDb.Metadata.MetadataDynamicFields;
COMMIT;

USE master
EXEC sp_detach_db 'DynamicFieldsMigration', 'true';