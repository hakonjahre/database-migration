IF DB_ID('MigrateApplicationParameters') IS NULL
    BEGIN
        PRINT 'Creating or attaching MigrateApplicationParameters'

        CREATE DATABASE MigrateApplicationParameters ON
            (
                FILENAME = 'E:\SQLDATA\MigrateApplicationParameters.mdf',
                NAME = 'MigrateApplicationParameters'
                )
    END
ELSE
    PRINT 'MigrateApplicataionParameters exists'
GO

-- Migrate data
BEGIN TRANSACTION
    PRINT 'Start migration'

    USE MigrateApplicationParameters;

    PRINT 'Migrating ApplicationTypes'
    SELECT *
    INTO ApplicationTypes
    FROM RSApplicationParametersDB.dbo.ApplicationTypes;

    PRINT 'Migrating Applications'
    SELECT *
    INTO Applications
    FROM RSApplicationParametersDB.dbo.Applications

    PRINT 'Migrating ApplicationChangedDynamicFieldSets'
    SELECT *
    INTO ApplicationChangedDynamicFieldSets
    FROM RSApplicationParametersDB.dbo.ApplicationChangedDynamicFieldSets;

    PRINT 'Migrating ApplicationChangedDynamicFields'
    SELECT *
    INTO ApplicationChangedDynamicFields
    FROM RSApplicationParametersDB.dbo.ApplicationChangedDynamicFields

    PRINT 'Migrating ApplicationDynamicFields'
    SELECT *
    INTO ApplicationDynamicFields
    FROM RSApplicationParametersDB.dbo.ApplicationDynamicFields

    PRINT 'Migrating Stores'
    SELECT StoreID,
           StoreName,
           StoreDisplayId,
           StoreExternalId,
           PublicOrgNumber,
           EANLocationNo,
           LastImportedMessageGuid,
           CreatedDate,
           ModifiedDate,
           ModifiedByUserId,
           HasUnexportedDataChanges,
           HasUnexportedHierarchyChanges,
           StateId
    INTO Stores
    FROM RSApplicationParametersDB.dbo.Stores


    PRINT 'Migrating CustomApplicationDynamicFields'
    SELECT A.ExternalId,
           A.ApplicationTypeNo,
           ADF.ApplicationDynamicFieldNo,
           ADF.DynamicFieldDefinitionId,
           ADF.DynamicFieldValue,
           ADF.ResolvedValue,
           ADF.OverriddenInApplicationConfig,
           SG.StoreGroupExternalId as InheritedFromStoreGroupExternalId,
           ADF.DynamicFieldGroupId,
           ADF.CreatedDate,
           ADF.CreatedBy,
           ADF.ModifiedDate,
           ADF.ModifiedBy,
           ADF.IsDeleted,
           ADF.DeletedDate,
           ADF.DeletedBy
    INTO ApplicationDynamicFieldsWithExternalId
    FROM RSApplicationParametersDB.dbo.ApplicationDynamicFields ADF
             JOIN RSMetadataESDb.DynamicFields.DynamicFieldDefinitions DFD
                  ON ADF.DynamicFieldDefinitionId = DFD.DynamicFieldDefinitionId
             JOIN RSApplicationParametersDB.dbo.Applications A
                  ON ADF.ApplicationNo = A.ApplicationNo
             JOIN RSApplicationParametersDB.dbo.StoreGroups SG
                  ON SG.StoreGroupName = ADF.InheritedFromStoreGroupName
                      AND SG.StoreGroupId = ADF.InheritedFromStoreGroupId
    ORDER BY ADF.ModifiedDate desc

    PRINT 'Migration finished'
COMMIT;
GO

USE master

EXEC sp_detach_db 'MigrateApplicationParameters', 'true';