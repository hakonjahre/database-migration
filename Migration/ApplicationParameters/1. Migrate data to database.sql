IF DB_ID('MigrateApplicationParameters') IS NULL
    BEGIN
        CREATE DATABASE MigrateApplicationParameters ON (
            FILENAME = 'E:\SQLDATA\MigrateApplicationParameters.mdf',
            NAME = 'MigrateApplicationParameters')
            FOR ATTACH;
    END;
GO

BEGIN TRANSACTION
    USE RSApplicationParametersDB;

    DECLARE ADFWEI_CUR CURSOR FOR
        SELECT A.ApplicationNo,
               ADF.APPLICATIONDYNAMICFIELDNO,
               ADFWEI.DYNAMICFIELDDEFINITIONID,
               ADFWEI.DYNAMICFIELDVALUE,
               ADFWEI.RESOLVEDVALUE,
               ADFWEI.OVERRIDDENINAPPLICATIONCONFIG,
               SG.StoreGroupId,
               SG.StoreGroupName,
               ADFWEI.DYNAMICFIELDGROUPID,
               ADFWEI.CREATEDDATE,
               ADFWEI.CREATEDBY,
               ADFWEI.MODIFIEDDATE,
               ADFWEI.MODIFIEDBY,
               ADFWEI.ISDELETED,
               ADFWEI.DELETEDDATE,
               ADFWEI.DeletedBy
        FROM MigrateApplicationParameters.dbo.ApplicationDynamicFieldsWithExternalId ADFWEI
                 JOIN Applications A
                      ON ADFWEI.ExternalId = A.ExternalId
                          AND A.ApplicationTypeNo = ADFWEI.ApplicationTypeNo
                 JOIN StoreGroups SG
                      ON SG.StoreGroupExternalId = ADFWEI.InheritedFromStoreGroupExternalId
                 LEFT JOIN ApplicationDynamicFields ADF
                           on A.ApplicationNo = ADF.ApplicationNo
                               AND ADF.DynamicFieldDefinitionId = ADFWEI.DynamicFieldDefinitionId

    DECLARE
        @adfApplicationNo                 int,
        @adfApplicationDynamicFieldNo     int,
        @adfDynamicFieldDefinitionId      varchar(255),
        @adfDynamicFieldValue             varchar(max),
        @adfResolvedValue                 varchar(max),
        @adfOverriddenInApplicationConfig bit,
        @adfInheritedFromStoreGroupId     varchar(100),
        @adfInheritedFromStoreGroupName   varchar(256),
        @adfDynamicFieldGroupId           varchar(50),
        @adfCreatedDate                   datetime,
        @adfCreatedBy                     varchar(255),
        @adfModifiedDate                  datetime,
        @adfModifiedBy                    varchar(255),
        @adfIsDeleted                     bit,
        @adfDeletedDate                   datetime,
        @adfDeletedBy                     varchar(255)

    OPEN ADFWEI_CUR;

    WHILE (1 = 1)
    BEGIN
        FETCH NEXT FROM ADFWEI_CUR INTO
            @adfApplicationNo,
            @adfApplicationDynamicFieldNo,
            @adfDynamicFieldDefinitionId,
            @adfDynamicFieldValue,
            @adfResolvedValue,
            @adfOverriddenInApplicationConfig,
            @adfInheritedFromStoreGroupId,
            @adfInheritedFromStoreGroupName,
            @adfDynamicFieldGroupId,
            @adfCreatedDate,
            @adfCreatedBy,
            @adfModifiedDate,
            @adfModifiedBy,
            @adfIsDeleted,
            @adfDeletedDate,
            @adfDeletedBy

        IF @@FETCH_STATUS <> 0 BREAK;


        DECLARE
            @adfPrimaryKey int,
            @debugNo       varchar(10) = isnull(convert(varchar, @adfApplicationDynamicFieldNo), 'unknown')

        IF @adfApplicationDynamicFieldNo IS NULL
            BEGIN
                PRINT @debugNo + ': Not matched'

                EXECUTE @adfPrimaryKey = usp_GetPrimaryKeyRange @TableName = 'ApplicationDynamicFields', @RangeSize = 1;

                INSERT INTO ApplicationDynamicFields (ApplicationNo,
                                                      ApplicationDynamicFieldNo,
                                                      DynamicFieldDefinitionId,
                                                      DynamicFieldValue,
                                                      ResolvedValue,
                                                      OverriddenInApplicationConfig,
                                                      InheritedFromStoreGroupId,
                                                      InheritedFromStoreGroupName,
                                                      DynamicFieldGroupId,
                                                      CreatedDate,
                                                      CreatedBy,
                                                      ModifiedDate,
                                                      ModifiedBy,
                                                      IsDeleted,
                                                      DeletedDate,
                                                      DeletedBy)
                SELECT @adfApplicationNo                 as ApplicationNo,
                       @adfPrimaryKey                    as ApplicationDynamicFieldNo,
                       @adfDynamicFieldDefinitionId      as DynamicFieldDefinitionId,
                       @adfDynamicFieldValue             as DynamicFieldValue,
                       @adfResolvedValue                 as ResolvedValue,
                       @adfOverriddenInApplicationConfig as OverriddenInApplicationConfig,
                       @adfInheritedFromStoreGroupId     as InheritedFromStoreGroupId,
                       @adfInheritedFromStoreGroupName   as InheritedFromStoreGroupName,
                       @adfDynamicFieldGroupId           as DynamicFieldGroupId,
                       @adfCreatedDate                   as CreatedDate,
                       @adfCreatedBy                     as CreatedBy,
                       @adfModifiedDate                  as ModifiedDate,
                       @adfModifiedBy                    as ModifiedBy,
                       @adfIsDeleted                     as IsDeleted,
                       @adfDeletedDate                   as DeletedDate,
                       @adfDeletedBy                     as DeletedBy
            END
        ELSE
            BEGIN
                PRINT @debugNo + ': Matched'
                UPDATE ApplicationDynamicFields
                SET OverriddenInApplicationConfig = @adfOverriddenInApplicationConfig,
                    ResolvedValue                 = @adfResolvedValue,
                    DynamicFieldValue             = @adfDynamicFieldValue,
                    InheritedFromStoreGroupName   = @adfInheritedFromStoreGroupName,
                    InheritedFromStoreGroupId     = @adfInheritedFromStoreGroupId,
                    IsDeleted                     = @adfIsDeleted,
                    ModifiedBy                    = 'Migration',
                    ModifiedDate                  = sysdatetime()
                WHERE ApplicationDynamicFieldNo = @adfApplicationDynamicFieldNo;
            END
    END

    CLOSE ADFWEI_CUR
    DEALLOCATE ADFWEI_CUR
ROLLBACK