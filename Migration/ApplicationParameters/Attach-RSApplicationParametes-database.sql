CREATE DATABASE RSApplicationParametersDB ON
    (FILENAME = '/tmp/RSApplicationParametersDB.mdf')
    LOG ON
    (FILENAME = '/tmp/RSApplicationParametersDB_log.ldf') FOR ATTACH;
GO;

USE RSApplicationParametersDB;
DBCC CHECKDB;

CREATE DATABASE MigrateApplicationParameters ON
    (FILENAME = '/tmp/MigrateApplicationParameters.mdf',
        NAME = 'MigrateApplicationParameters')
--     LOG ON
--     (FILENAME = '/tmp/MigrateApplicationParameters_log.ldf')
    FOR ATTACH;
GO;

USE MigrateApplicationParameters;
DBCC CHECKDB;